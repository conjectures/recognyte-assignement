# Create kubernetes configs


## Prerequisites 
- Make sure that the deployment image `nexus-gcs` is available in your project in GCP
- Change the project in the `nexus-deployment.yaml` config file under the container image flag for the deployment
- Run in a GKE cluster

## Instructions
- Download files from repo
- If image is not in local GCR repository, push it with these commands:
```sh
docker build -t nexus-gcs -f src/Dockerfile .
docker tag nexus-gcs eu.gcr.io/<project-id>/nexus-gcs:latest
docker push eu.gcr.io/<project-id>/nexus-gcs:latest
```

- Run with
```sh
kubectl apply -f nexus-deployment.yaml
```



## Todo
- Helm Chart
