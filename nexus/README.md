# Build Custom Container

Build container with: 
```sh
docker build -t nexus-gcs -f src/Dockerfile .
```
Run container with:
```sh
docker run --name nexus -d -p <host-port>:8081 nexus-gcs
```
where host-port can be any port that can be reached from the browser.
Wait until the *nexus* database initialises. Optionally watch progress with `docker logs -f nexus`


Optionally, push the image to *Container Registry*
```sh
# in google shell
docker tag nexus-gcs gcr.io/<your-project>/nexus-gcs:latest
docker push gcr.io/<your-project>/nexus-gcs:latest
```
