# Create bucket
# https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket
resource "google_storage_bucket" "static-site" {
  project       = var.project_id
  name          = "${var.project_id}-storage"
  location      = "EU"
  force_destroy = true
}
