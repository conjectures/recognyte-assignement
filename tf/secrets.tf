
# Generate key for Storage Service Account
# https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/google_service_account_key
resource "google_service_account_key" "storage-sa-key"{
  service_account_id = google_service_account.service-storage.name
  public_key_type = "TYPE_X509_PEM_FILE" # default

  depends_on = [
      google_service_account.service-storage
  ]
}

# Create a secret for the storage service account
# https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/secret_manager_secret
resource "google_secret_manager_secret" "storage-sa-secret" {
  secret_id = "storage-sa-key"

  labels = {
    type = "sa-key"
  }

  replication {
    automatic = true
  }
  project = var.project_id
  depends_on = [
      google_project_service.secretmanager
  ]
}

# Create a new version of the secret and store the key
# https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/secret_manager_secret_version
resource "google_secret_manager_secret_version" "storage-sa-key" {
  secret = google_secret_manager_secret.storage-sa-secret.id
  secret_data = base64decode(google_service_account_key.storage-sa-key.private_key)
}



