

# VPC
# https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_network
resource "google_compute_network" "vpc" {
  name                    = "${var.project_id}-vpc"
  routing_mode            = "REGIONAL"
  auto_create_subnetworks = "false"

  # Enable apis first
  depends_on = [
    google_project_service.compute,
    google_project_service.container
  ]
}

# Subnet (private)
# https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_subnetwork
resource "google_compute_subnetwork" "subnet" {
  name          = "${var.project_id}-subnet"
  region        = var.region
  network       = google_compute_network.vpc.name
  ip_cidr_range = "10.10.0.0/24" # ip range for resources in subnet

  # IP range allocated to pods in cluster
  secondary_ip_range {
    range_name = "k8s-pod-ip-range"
    ip_cidr_range = "10.32.0.0/20"
  }
  # IP range allocated to Cluster IPs
  secondary_ip_range {
    range_name = "k8s-service-ip-range"
    ip_cidr_range = "10.52.0.0/20"
  }
}
