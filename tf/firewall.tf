# Add firewall rule to ServiceAccount since node tag is unknown until creation
# https://stackoverflow.com/questions/60744761/adding-firewall-rule-for-gke-nodes
resource "google_compute_firewall" "kubernetes-nodeport-firewall" {
  name    = "kubernetes-nodeport-firewall-50000"
  network = google_compute_network.vpc.name

  allow {
    protocol = "tcp"
    ports    = ["30081"]
  }

  source_service_accounts = [google_service_account.kubernetes-sa.email]

}
