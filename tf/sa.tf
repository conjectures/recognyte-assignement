# Service Accounts
# https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/google_service_account

# Create service account for kubernetes to access GCR
resource "google_service_account" "kubernetes-sa" {
  account_id = "kubernetes-sa"
  display_name = "Kubernetes Service Account"
}

# Grant storage admin role to service account
# https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/google_project_iam
resource "google_project_iam_member" "kubernetesa-additive-storage" {
  project = var.project_id
  role = "roles/storage.admin"
  member = "serviceAccount:${google_service_account.kubernetes-sa.email}"
}
resource "google_project_iam_member" "kubernetes-additive-compute" {
  project = var.project_id
  role = "roles/compute.admin"
  member = "serviceAccount:${google_service_account.kubernetes-sa.email}"
}
# Grant registry viewership to kubernetes node to fix permission denied to registry error
# https://cloud.google.com/kubernetes-engine/docs/troubleshooting#permission_denied_error
resource "google_project_iam_member" "kubernetes-additive-registry" {
  project = var.project_id
  role = "roles/artifactregistry.reader"
  member = "serviceAccount:${google_service_account.kubernetes-sa.email}"
}



# Create service account for pod to access GCS and grant necessary access
resource "google_service_account" "service-storage" {
  account_id = "service-storage"
  display_name = "Storage Service Account"
}

resource "google_project_iam_member" "storage-sa-additive" {
  project = var.project_id
  role = "roles/storage.admin"
  member = "serviceAccount:${google_service_account.service-storage.email}"
}

# Grant impersonation rights to kubernetes service account
resource "google_service_account_iam_member" "storage-sa-kubernetes" {
  service_account_id  = google_service_account.service-storage.id
  role                = "roles/iam.workloadIdentityUser"
  member              = "serviceAccount:${var.project_id}.svc.id.goog[default/service-storage]"

  depends_on = [
    google_container_cluster.primary
  ]
}



