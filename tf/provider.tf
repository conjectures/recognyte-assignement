# Set up provider
# https://registry.terraform.io/providers/hashicorp/google/latest/docs
provider "google" {

  project =   var.project_id
  region  =   var.region
  zone    =   var.zone
  # credentials = file("<key>.json")
}


# Backend to store in gcs (uncomment)
# https://developer.hashicorp.com/terraform/language/settings/backends/configuration
# terraform {
#   backend "gcs" {
#     bucket  = "tf-state-prod"
#     prefix  = "terraform/state"
#   }
# }
