# GKE Cluster with Terraform Example

## Instructions 
The instructions will walk you through to create a new project in GCP with a GKE cluster from the cloud shell.
If the cluster is to be created in an existing project, the authentication needs to be with the context of the targeted project (see below)

### Summary
1. Log in to GCP and create a new project
2. Authenticate with ADC (Application Default Credentials)
3. Export environment variable `$GOOGLE_APPLICATION_CREDENTIALS` pointing to generated file
4. Download files from git 
5. Navigate to the terraform project and run

### 1. Login to GCP and create a new project
The new project can be created with the UI or in cloud shell

### 2. Authenticate with ADC
On cloud shell run 
```sh
gcloud auth application-default login --project <project-id>
```
where `<project-id>` is the newly created project.

### 3. Export environment variable 
Run command
```sh
export GOOGLE_APPLICATION_CREDENTIALS=$HOME/.config/gcloud/application_default_credentials.json
```
### 4. Download repo files
```sh
git clone -var project_id="<PROJECT_ID>"
```
### 5. Navigate to the terraform project and run
Change directory to the terraform project `recognyte-task/task03/gke`.
Run with
```sh
terraform apply -var project_id="<PROJECT_ID>"
```

### Optional: Get access to cluster and validate
6. Get access to cluster:
```sh
gcloud container clusters get-credentials <cluster-name> --region europe-west1 --project <project-id>
```

## Assumptions

If the code is run from a VM in a GCP project, the following services should be enabled:
- `cloudresourcemanager.googleapis.com`

## TODO
- [Create service account for nexus](https://github.com/sonatype-nexus-community/nexus-blobstore-google-cloud/blob/main/docker-compose.yml#google-cloud-services-and-iam-roles) 
- Create service account for kubernetes (pull image)
https://blog.container-solutions.com/using-google-container-registry-with-kubernetes
