# Setting requirements for terraform and provider
# https://developer.hashicorp.com/terraform/tutorials/configuration-language/versions
# https://developer.hashicorp.com/terraform/language/providers/requirements
terraform {
  required_version = ">= 0.14"

  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "~> 4.0"
    }
  }

}
